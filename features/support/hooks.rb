Before do
    Capybara.current_session.driver.browser.manage.delete_all_cookies
    page.reset_session!
    Capybara.current_session.driver.quit
  end

After do
    shot_file = page.save_screenshot("log/screenshot.png")
    shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
    embed(shot_b64, "image/png", "Screenshot") # Cucumber anexa o screenshot no report
    page.execute_script("sessionStorage.clear()")

end