
#language:pt

Funcionalidade: Cadastro de Usuário

@create
Cenário: Cadastro de usuário com sucesso

Dado que acesso a pagina principal
Quando criar uma nova conta
E preencher as informações "Informação Pessoal", "Informações de Login"
E clicar em "Create an Account"
Então devo ver a mensagem "Thank you for registering with Main Website Store".