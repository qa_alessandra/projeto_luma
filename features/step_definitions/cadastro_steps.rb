Dado('que acesso a pagina principal') do
  visit 'https://magento.nublue.co.uk/'
end

Quando('criar uma nova conta') do
  click_link_or_button("Create an Account")
end

Quando('preencher as informações {string}, {string}') do |string, string2|
  numero = rand(9999)
  fill_in('firstname', :with => 'Junior')
  fill_in('lastname', :with => 'Castro')
  fill_in('email_address', :with => "junior.qa'#{numero}'@teste.com.br")
  fill_in('password', :with => '123@mudar')
  fill_in('password-confirmation', :with => '123@mudar')
end

Quando('clicar em {string}') do |variavel|
  find("button[title='Create an Account']").click
end

Então('devo ver a mensagem {string}.') do |mensagem|
  expect(page).to have_text mensagem
end


