Dado('que acesso a pagina {string} e pagina de login {string}') do |luma, string2|
    visit 'https://magento.nublue.co.uk/'
    click_link('Sign In')
end
  
Dado('faço o login com {string} e senha {string}') do |email, senha|
    find('#email').set email
    find('#pass').set senha
end

Dado('clico em {string}') do |variavel|
    find('#send2').click
    sleep 5
end
  
Quando('pesquisar e adicionar um produto no carrinho') do
    find("img[alt='Radiant Tee']").click
    find('#option-label-size-142-item-169').click
    find('#option-label-color-93-item-57').click
    find('#product-addtocart-button').click
    click_link('shopping cart')
end
   
Quando('finalizar o pedido') do
    sleep 3
    find("button[title='Proceed to Checkout']").click
end
  
Quando('confimar endereço e metodo de envio') do
    assert_no_selector(".loading-mask", wait: 10)
    verifica_endereco = has_css?("input[name='firstname']", wait: 3)
    
    if verifica_endereco == true 
        find("input[name='firstname']").set "Alessandra"
        find("input[name='street[0]']").set "Godoy"
        find("input[name='city']").set "Pinheiros"
        find("select[name='country_id'] option[value='BR']").click
        find("select[name='region_id'] option[value='508']").click
        find("input[name='postcode']").set "04543-000"
        find("input[name='telephone']").set "11951044444"
    end
    assert_no_selector(".loading-mask", wait: 10)
    first("input[class='radio']").click
    find("button[data-role='opc-continue']").click 
end
  
Quando('confirmar forma de pagamento') do
    assert_no_selector(".loading-mask", wait: 10)
    find("button[class='action primary checkout']").click

end
  
Então('pedido é gerado com sucesso') do
   expect(page).to have_content 'Thank you for your purchase!'
   sleep 5
end
